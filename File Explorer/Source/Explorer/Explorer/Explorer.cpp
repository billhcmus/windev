﻿// Explorer.cpp : Defines the entry point for the application.
//

#include "stdafx.h"
#include "Explorer.h"
#include "TreeView.h"
#include "ListView.h"
#define MAX_LOADSTRING 100

// Global Variables:
HINSTANCE hInst;								// current instance
TCHAR szTitle[MAX_LOADSTRING];					// The title bar text
TCHAR szWindowClass[MAX_LOADSTRING];			// the main window class name

// Forward declarations of functions included in this code module:
ATOM				MyRegisterClass(HINSTANCE hInstance);
BOOL				InitInstance(HINSTANCE, int);
LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
INT_PTR CALLBACK	About(HWND, UINT, WPARAM, LPARAM);

int APIENTRY _tWinMain(_In_ HINSTANCE hInstance,
                     _In_opt_ HINSTANCE hPrevInstance,
                     _In_ LPTSTR    lpCmdLine,
                     _In_ int       nCmdShow)
{
	UNREFERENCED_PARAMETER(hPrevInstance);
	UNREFERENCED_PARAMETER(lpCmdLine);

 	// TODO: Place code here.
	MSG msg;
	HACCEL hAccelTable;

	// Initialize global strings
	LoadString(hInstance, IDS_APP_TITLE, szTitle, MAX_LOADSTRING);
	LoadString(hInstance, IDC_EXPLORER, szWindowClass, MAX_LOADSTRING);
	MyRegisterClass(hInstance);

	// Perform application initialization:
	if (!InitInstance (hInstance, nCmdShow))
	{
		return FALSE;
	}

	hAccelTable = LoadAccelerators(hInstance, MAKEINTRESOURCE(IDC_EXPLORER));

	// Main message loop:
	while (GetMessage(&msg, NULL, 0, 0))
	{
		if (!TranslateAccelerator(msg.hwnd, hAccelTable, &msg))
		{
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		}
	}

	return (int) msg.wParam;
}



//
//  FUNCTION: MyRegisterClass()
//
//  PURPOSE: Registers the window class.
//
ATOM MyRegisterClass(HINSTANCE hInstance)
{
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style			= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc	= WndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance		= hInstance;
	wcex.hIcon			= LoadIcon(hInstance, MAKEINTRESOURCE(IDI_EXPLORER));
	wcex.hCursor		= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName	= MAKEINTRESOURCE(IDC_EXPLORER);
	wcex.lpszClassName	= szWindowClass;
	wcex.hIconSm		= LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	return RegisterClassEx(&wcex);
}

//
//   FUNCTION: InitInstance(HINSTANCE, int)
//
//   PURPOSE: Saves instance handle and creates main window
//
//   COMMENTS:
//
//        In this function, we save the instance handle in a global variable and
//        create and display the main program window.
//
BOOL InitInstance(HINSTANCE hInstance, int nCmdShow)
{
   HWND hWnd;

   hInst = hInstance; // Store instance handle in our global variable

   hWnd = CreateWindow(szWindowClass, szTitle, WS_OVERLAPPEDWINDOW,
      CW_USEDEFAULT, 0, CW_USEDEFAULT, 0, NULL, NULL, hInstance, NULL);

   if (!hWnd)
   {
      return FALSE;
   }

   ShowWindow(hWnd, nCmdShow);
   UpdateWindow(hWnd);

   return TRUE;
}

//
//  FUNCTION: WndProc(HWND, UINT, WPARAM, LPARAM)
//
//  PURPOSE:  Processes messages for the main window.
//
//  WM_COMMAND	- process the application menu
//  WM_PAINT	- Paint the main window
//  WM_DESTROY	- post a quit message and return
//
//

HWND g_TreeView;
HWND g_ListView;
Drive* g_Drive;
int nCurSelIndex;
NMHDR* notifyMess;
LPNMTREEVIEW lpnmTreeView;
HTREEITEM curSelected;


LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int wmId, wmEvent;
	PAINTSTRUCT ps;
	HDC hdc;
	
	// UI element
	RECT rcParent;
	LPWSTR rs;
	
	long style;
	switch (message)
	{
	case WM_CREATE:

		InitCommonControls();

		GetClientRect(hWnd, &rcParent);
		style = TVS_HASLINES | TVS_LINESATROOT | TVS_HASBUTTONS | TVS_SHOWSELALWAYS;
		
		//init Drive variable 
		g_Drive = new Drive;
		g_TreeView = CreateATreeView(hWnd, ID_TREEVIEW, hInst, 0, 0, 0, (rcParent.right - rcParent.left) / 4, (rcParent.bottom - rcParent.top), style);
		g_Drive->GetSystemDrives();
		loadThisPCToTree(g_Drive, g_TreeView);

		style = LVS_REPORT | LVS_ICON | LVS_EDITLABELS | LVS_SHOWSELALWAYS;

		g_ListView = CreateAListView(hWnd, ID_LISTVIEW, hInst, WS_EX_CLIENTEDGE, ((rcParent.right - rcParent.left) / 4), 0, (rcParent.right - rcParent.left) * 3 / 4, rcParent.bottom - rcParent.top, style);
		loadThisPCToList(g_Drive, g_ListView);
		break;
	case WM_SIZE:
		GetClientRect(hWnd, &rcParent);
		MoveWindow(g_TreeView, 0, 0, rcParent.right / 4, rcParent.bottom, TRUE);
		MoveWindow(g_ListView, ((rcParent.right - rcParent.left) / 4), 0, rcParent.right * 3 / 4, rcParent.bottom, TRUE);
		break;
	case WM_COMMAND:
		wmId    = LOWORD(wParam);
		wmEvent = HIWORD(wParam);
		// Parse the menu selections:
		switch (wmId)
		{
		case IDM_ABOUT:
			DialogBox(hInst, MAKEINTRESOURCE(IDD_ABOUTBOX), hWnd, About);
			break;
		case IDM_EXIT:
			DestroyWindow(hWnd);
			break;
		default:
			return DefWindowProc(hWnd, message, wParam, lParam);
		}
		break;
	case WM_NOTIFY:
		notifyMess = (NMHDR*)lParam;
		lpnmTreeView = (LPNMTREEVIEW)notifyMess;
		HTREEITEM curSelected; // giữ giá trị đường dẫn
		switch (notifyMess->code) {
		case TVN_ITEMEXPANDING:
			curSelected = lpnmTreeView->itemNew.hItem; // lấy đường dẫn hiện tại
			ExpandChild(g_TreeView, curSelected);
			break;
		case TVN_SELCHANGED: // thay đổi khi seclect item
			curSelected = TreeView_GetSelection(g_TreeView);
			TreeView_Expand(g_TreeView, curSelected, TVE_EXPAND);

			ListView_DeleteAllItems(g_ListView); // xóa cái list view hiện thời để load cái mới, với đường dẫn ở tree view -> tạo sự liên kết
			LoadListViewItem(GetPath(curSelected, g_TreeView), g_ListView, g_Drive);
			break;
		case NM_CLICK:
			break;
		case NM_DBLCLK:
			if (notifyMess->hwndFrom == g_ListView){ // nếu thấy double click từ list view
				if (ListView_GetSelectionMark(g_ListView) != -1) { // không có cái này thì click vào vùng trống sẽ văng exception vì path không hợp lệ
					OpenOrExecuteSelected(g_ListView);
				}
			}
			break;
		}
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		// TODO: Add any drawing code here...
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY:
		PostQuitMessage(0);
		break;
	default:
		return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}

// Message handler for about box.
INT_PTR CALLBACK About(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
{
	UNREFERENCED_PARAMETER(lParam);
	switch (message)
	{
	case WM_INITDIALOG:
		return (INT_PTR)TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL)
		{
			EndDialog(hDlg, LOWORD(wParam));
			return (INT_PTR)TRUE;
		}
		break;
	}
	return (INT_PTR)FALSE;
}
