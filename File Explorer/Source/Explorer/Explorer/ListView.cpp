﻿#include "stdafx.h"
#include "ListView.h"

#define MAXLEN 10240

HINSTANCE dllhinst = NULL;

BOOL InitListViewImageLists(HWND hWndListView)
{
	// MSDN T-T
	HICON hIcon;
	HIMAGELIST hLarge;   // Image list for icon view.
	HIMAGELIST hSmall;   // Image list for other views.

	// Create the full-sized icon image lists. 
	hLarge = ImageList_Create(GetSystemMetrics(SM_CXICON), GetSystemMetrics(SM_CYICON), ILC_COLOR32, 1, 1);
	ImageList_SetBkColor(hLarge, GetSysColor(COLOR_WINDOW));

	hSmall = ImageList_Create(GetSystemMetrics(SM_CXSMICON), GetSystemMetrics(SM_CYSMICON), ILC_COLOR32, 1, 1);
	ImageList_SetBkColor(hSmall, GetSysColor(COLOR_WINDOW));

	dllhinst = LoadLibrary(L"shell32.dll");

	// thứ 0 là ThisPC
	hIcon = LoadIcon(dllhinst, MAKEINTRESOURCE(16));
	ImageList_AddIcon(hLarge, hIcon);
	ImageList_AddIcon(hSmall, hIcon);
	// 1 -> Local Disk
	hIcon = LoadIcon(dllhinst, MAKEINTRESOURCE(9));
	ImageList_AddIcon(hLarge, hIcon);
	ImageList_AddIcon(hSmall, hIcon);
	// 2-> Folder
	hIcon = LoadIcon(dllhinst, MAKEINTRESOURCE(4));
	ImageList_AddIcon(hLarge, hIcon);
	ImageList_AddIcon(hSmall, hIcon);
	// 3-> USB
	hIcon = LoadIcon(dllhinst, MAKEINTRESOURCE(8));
	ImageList_AddIcon(hLarge, hIcon);
	ImageList_AddIcon(hSmall, hIcon);
	// 4-> Network Drive
	hIcon = LoadIcon(dllhinst, MAKEINTRESOURCE(10));
	ImageList_AddIcon(hLarge, hIcon);
	ImageList_AddIcon(hSmall, hIcon);
	// 5-> CD-ROM
	hIcon = LoadIcon(dllhinst, MAKEINTRESOURCE(12));
	ImageList_AddIcon(hLarge, hIcon);
	ImageList_AddIcon(hSmall, hIcon);
	// 6-> UNKNOWN_FILE
	hIcon = LoadIcon(dllhinst, MAKEINTRESOURCE(1));
	ImageList_AddIcon(hLarge, hIcon);
	ImageList_AddIcon(hSmall, hIcon);

	DestroyIcon(hIcon);
	ListView_SetImageList(hWndListView, hLarge, LVSIL_NORMAL);
	ListView_SetImageList(hWndListView, hSmall, LVSIL_SMALL);
	return TRUE;
}

HWND CreateAListView(HWND hwndParent, long ID, HINSTANCE hInst, long lExtStyle, int x, int y, int nWidth, int nHeight, long lStyle) {
	HWND hListView = CreateWindowEx(lExtStyle, WC_LISTVIEW, L"List View", WS_CHILD | WS_VISIBLE | WS_VSCROLL | WS_TABSTOP | lStyle, x, y, nWidth, nHeight, hwndParent, (HMENU)ID, hInst, 0);

	InitListViewImageLists(hListView);
	InitBasicCol(hListView);


	return hListView;
}

void loadThisPCToList(Drive* drive, HWND hListView) {
	InitDriveCol(hListView);

	LV_ITEM lv;

	for (int i = 0; i < drive->getCount(); i++) {
		// Cột đầu tiên
		lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
		lv.iItem = i;
		lv.iSubItem = 0;
		lv.pszText = drive->getDisplayName(i);

		// Hơi chuối T-T
		TCHAR* checktype = new TCHAR[15];
		checktype = drive->GetType(i);
		if (StrCmp(checktype, DR_FIXED) == 0) {
			lv.iImage = IDI_LOCALDISK;
		}
		else if (StrCmp(checktype, DR_REMOVABLE) == 0) {
			lv.iImage = IDI_USB;
		}
		else if (StrCmp(checktype, DR_REMOTE) == 0) {
			lv.iImage = IDI_NETWORK_DRIVE;
		}
		else if (StrCmp(checktype, DR_CDROM) == 0) {
			lv.iImage = IDI_CDROM;
		}
		lv.lParam = (LPARAM)drive->getDriveLetter(i);
		ListView_InsertItem(hListView, &lv);

		// Cột tiếp theo là type, total space, free space
		lv.mask = LVIF_TEXT;
		lv.iSubItem = 1;
		lv.pszText = drive->GetType(i);

		ListView_SetItem(hListView, &lv);

		// total space
		lv.iSubItem = 2;
		if (drive->GetType(i) != DR_CDROM && drive->GetType(i) != DR_REMOTE) {
			lv.pszText = drive->TotalSizeToStr(i);
		}
		else {
			lv.pszText = NULL;
		}
		ListView_SetItem(hListView, &lv);

		// free space
		lv.iSubItem = 3;
		if (drive->GetType(i) != DR_CDROM && drive->GetType(i) != DR_REMOTE) {
			lv.pszText = drive->FreeSizeToStr(i);
		}
		else {
			lv.pszText = NULL;
		}
		ListView_SetItem(hListView, &lv);
	}
}

void InitBasicCol(HWND hListView) {
	// 4 cột Name, Type, Total Size, Free Space
	LVCOLUMN lvCol;

	// set mặt nạ
	lvCol.mask = LVCF_FMT | LVCF_TEXT | LVCF_WIDTH;

	lvCol.fmt = LVCFMT_LEFT;
	lvCol.cx = 200;
	lvCol.pszText = L"Name";
	ListView_InsertColumn(hListView, 0, &lvCol);

	lvCol.fmt = LVCFMT_LEFT;
	lvCol.pszText = L"Type";
	lvCol.cx = 150;
	ListView_InsertColumn(hListView, 1, &lvCol);

	lvCol.fmt = LVCFMT_LEFT;
	lvCol.pszText = L"Size";
	ListView_InsertColumn(hListView, 2, &lvCol);

	lvCol.fmt = LVCFMT_LEFT;
	lvCol.pszText = L"Description";
	ListView_InsertColumn(hListView, 3, &lvCol);
}

void InitDriveCol(HWND hListView) {
	LVCOLUMN lvCol;

	lvCol.mask = LVCF_TEXT | LVCF_FMT;

	lvCol.fmt = LVCFMT_LEFT;
	lvCol.cx = 200;
	lvCol.pszText = L"Type";
	ListView_SetColumn(hListView, 1, &lvCol);

	lvCol.fmt = LVCFMT_LEFT;
	lvCol.cx = 100;
	lvCol.pszText = L"Total Space";
	ListView_SetColumn(hListView, 2, &lvCol);

	lvCol.fmt = LVCFMT_LEFT;
	lvCol.cx = 100;
	lvCol.pszText = L"Free Space";
	ListView_SetColumn(hListView, 3, &lvCol);
}

void InitFolderCol(HWND hListView) {
	LVCOLUMN lvCol;

	lvCol.mask = LVCF_TEXT | LVCF_FMT;
	lvCol.fmt = LVCFMT_LEFT;
	lvCol.pszText = L"Date modified";
	ListView_SetColumn(hListView, 1, &lvCol);

	lvCol.mask = LVCF_TEXT | LVCF_FMT;
	lvCol.fmt = LVCFMT_LEFT;
	lvCol.cx = 130;
	lvCol.pszText = L"Type";
	ListView_SetColumn(hListView, 2, &lvCol);

	lvCol.fmt = LVCFMT_LEFT;
	lvCol.pszText = L"Size";
	ListView_SetColumn(hListView, 3, &lvCol);
}

void LoadListViewItem(LPCWSTR path, HWND hListView, Drive* drive) {
	//LV_ITEM lv;

	if (StrCmp(path, L"This PC") == 0) { // đang ở This PC
		InitDriveCol(hListView); // khởi tạo list view theo dạng ổ đĩa
		loadThisPCToList(drive, hListView);
	}
	else {
		LoadFileAndFolder(hListView, path);
	}
}


void LoadFileAndFolder(HWND hListView, LPCWSTR path) {
	if (path == NULL)
		return;

	InitFolderCol(hListView);
	TCHAR buffer[MAXLEN];

	StrCpy(buffer, path);

	if (wcslen(path) == 3) { // quét ổ đĩa
		StrCat(buffer, L"*");
	}
	else { // quét thư mục
		StrCat(buffer, L"\\*");
	}

	// Tìm file, folder trong thu mục
	WIN32_FIND_DATA fd;
	HANDLE hFile;
	LV_ITEM lv;
	TCHAR* folderPath;
	int ItemCount = 0;

	// Lấy thư mục lần đầu
	hFile = FindFirstFileW(buffer, &fd);
	if (hFile == INVALID_HANDLE_VALUE) {
		return;
	}

	do {
		// bỏ qua mấy loại file ẩn, file .,..
		if ((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) &&
			((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN) &&
			(StrCmp(fd.cFileName, L".") != 0) && (StrCmp(fd.cFileName, L"..") != 0))
		{
			folderPath = new TCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];
			StrCpy(folderPath, path);

			if (wcslen(path) != 3) { // không phải đĩa
				StrCat(folderPath, L"\\");
			}
			StrCat(folderPath, fd.cFileName);

			lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
			lv.iItem = ItemCount;
			lv.iSubItem = 0;
			lv.pszText = fd.cFileName;
			lv.lParam = (LPARAM)folderPath;
			// tạm thời bỏ qua icon
			lv.iImage = IDI_FOLDER;
			ListView_InsertItem(hListView, &lv);
			// Cột ngày chỉnh sửa
			ListView_SetItemText(hListView, ItemCount, 1, getDateModified(fd.ftLastWriteTime));
			// Cột loại
			ListView_SetItemText(hListView, ItemCount, 2, L"File Folder");
			ItemCount++;
		}
	} while (FindNextFileW(hFile, &fd));

	// Chạy lần 2 để lấy tệp tin
	TCHAR* filePath;

	hFile = FindFirstFileW(buffer, &fd);
	if (hFile == INVALID_HANDLE_VALUE) {
		return;
	}

	do {
		// Hiển thị file, không hiển thị file hệ thống, file ẩn
		if (((fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) != FILE_ATTRIBUTE_DIRECTORY) &&
			((fd.dwFileAttributes & FILE_ATTRIBUTE_SYSTEM) != FILE_ATTRIBUTE_SYSTEM) &&
			((fd.dwFileAttributes & FILE_ATTRIBUTE_HIDDEN) != FILE_ATTRIBUTE_HIDDEN))
		{
			filePath = new TCHAR[wcslen(path) + wcslen(fd.cFileName) + 2];
			StrCpy(filePath, path); // đổ qua

			if (wcslen(path) != 3) {
				// không ở this pc thì add cái này vô
				StrCat(filePath, L"\\");
			}
			StrCat(filePath, fd.cFileName);

			// Hiển thị cột đầu là tên
			lv.mask = LVIF_TEXT | LVIF_IMAGE | LVIF_PARAM;
			lv.iItem = ItemCount;
			lv.iSubItem = 0;
			lv.pszText = fd.cFileName;
			lv.iImage = IDI_UNKNOW_FILE;
			lv.lParam = (LPARAM)filePath;
			// tạm thời bỏ qua image
			ListView_InsertItem(hListView, &lv);
			// Date modified
			ListView_SetItemText(hListView, ItemCount, 1, getDateModified(fd.ftLastWriteTime));
			// Type -> Tạm thời bỏ qua
			ListView_SetItemText(hListView, ItemCount, 2, GetType(fd));
			DWORD size = fd.nFileSizeLow; // kích thước file dạng bytes
			ListView_SetItemText(hListView, ItemCount, 3, Convert(size));
			ItemCount++;
		}
	} while (FindNextFileW(hFile, &fd));
}

LPCWSTR getPath(HWND hListView, int Item) {
	LVITEM lv;
	lv.mask = LVIF_PARAM;
	lv.iItem = Item;
	lv.iSubItem = 0;
	ListView_GetItem(hListView, &lv);
	return (LPCWSTR)lv.lParam;
}

void OpenOrExecuteSelected(HWND hListView) {

	LPCWSTR path = getPath(hListView, ListView_GetSelectionMark(hListView));
	WIN32_FIND_DATA fd;
	GetFileAttributesEx(path, GetFileExInfoStandard, &fd);

	if (fd.dwFileAttributes & FILE_ATTRIBUTE_DIRECTORY) {
		// nếu là thư mục
		ListView_DeleteAllItems(hListView); // clear
		LoadFileAndFolder(hListView, path);
	}
	else {
		// chạy file đó
		ShellExecute(NULL, L"Open", path, NULL, NULL, SW_SHOWNORMAL);
	}

}

LPWSTR getDateModified(const FILETIME &time) {
	SYSTEMTIME SysTime;
	// chuyển file time sang system time
	FileTimeToSystemTime(&time, &SysTime);
	char date[255], Time[255];
	GetDateFormat(LOCALE_USER_DEFAULT, DATE_AUTOLAYOUT, &SysTime, NULL, (LPWSTR)date, 255);
	GetTimeFormat(LOCALE_USER_DEFAULT, 0, &SysTime, NULL, (LPWSTR)Time, 255);

	//convert to wcstring
	TCHAR* buffer = new TCHAR[100];
	wsprintf(buffer, L"%s %s", date, Time);
	return buffer;
}

WCHAR* GetType(WIN32_FIND_DATA &fd) {
	// nguồn https://stackoverflow.com/questions/20634666/get-a-mime-type-from-a-extension-in-c
	LPWSTR fileName = fd.cFileName;
	int len = wcslen(fileName);
	int pos = len - 1;
	for (; pos >= 0 && fileName[pos] != '.'; pos--); // tìm pos của '.'
	if (pos < 0) {
		return L"Unknown";
	}
	WCHAR* szExtension = new WCHAR[len - pos + 1];
	StrCpy(szExtension, &fileName[pos]); // lấy đoạn extension
	

	HKEY hKey = NULL;
	WCHAR* szResult = new WCHAR[256];
	StrCpy(szResult, L"Unknown");
	if (RegOpenKeyEx(HKEY_CLASSES_ROOT, szExtension, 0, KEY_READ, &hKey) == ERROR_SUCCESS) {
		// buffer
		WCHAR szBuffer[256] = { 0 };
		DWORD dwBufferSize = sizeof(szBuffer);
		// lấy kiểu
		if (RegQueryValueEx(hKey, L"", NULL, NULL, (LPBYTE)szBuffer, &dwBufferSize) == ERROR_SUCCESS) {
			StrCpy(szResult, szBuffer);
		}
		// close key
		RegCloseKey(hKey);
	}
	return szResult;
}