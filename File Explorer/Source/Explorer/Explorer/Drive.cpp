﻿#include "stdafx.h" // skips any include before stdafx @@
#include "Drive.h"

#pragma comment(lib, "shlwapi.lib")

#define BUFFER_LEN 105
// enough dangerous
#define VOLUME_SERIAL_NUMBER NULL
#define MAXIMUM_COMPONENT_LENGTH NULL
#define FILE_SYSTEM_FLAGS NULL
#define FILE_SYSTEM_NAME_BUFFER NULL
#define FILE_SYSTEM_NAME_SIZE 0



LPWSTR Convert(_int64 iSize) {
	int iType = 0; // 1 kb; 2 mb; 3 gb; 4 tb
	int rightpart;
	while (iSize >= 1048576) {
		iSize /= 1024;
		iType++;
	}
	if (iSize > 1024) {

		rightpart = iSize % 1024; // lấy phần dư, là phần thập phân sau này

		while (rightpart > 99){
			rightpart /= 102;
		}
		iSize /= 1024; // đơn vị cao nhất có thể đổi ra từ bytes
		iType++;
	}

	TCHAR* rs = new TCHAR[12]; // chuỗi chứa dung lượng đĩa

	_itow(iSize, rs, 10);
	if (rightpart > 0 && iType > 1){
		wcscat(rs, L".");
		TCHAR* right = new TCHAR[5];
		_itow(rightpart, right, 10);
		wcscat(rs, right);
	}
	switch (iType){
	case 0: // bytes
		wcscat(rs, L" bytes");
		break;
	case 1:
		wcscat(rs, L" KB");
		break;
	case 2:
		wcscat(rs, L" MB");
		break;
	case 3:
		wcscat(rs, L" GB");
		break;
	case 4:
		wcscat(rs, L" TB");
		break;
	}
	return rs;
}


int Drive::getCount() {
	return this->nCount;
}

TCHAR* Drive::getDriveLetter(const int &i) {
	return this->pszDriveLetter[i];
}

TCHAR* Drive::getVolumeLabel(const int &i) {
	return this->pszVolumeLabel[i];
}

TCHAR* Drive::getDisplayName(const int &i) {
	return this->pszDisplayName[i];
}

TCHAR* Drive::GetType(const int &i) {
	return this->pszDriveType[i];
}

__int64 Drive::GetSize(const int &i) {
	__int64 rs;
	SHGetDiskFreeSpaceEx(this->pszDriveLetter[i], NULL, (PULARGE_INTEGER)&rs, NULL);
	return rs;
}

LPWSTR Drive::TotalSizeToStr(const int &i) {
	return Convert(GetSize(i));
}

__int64 Drive::GetFreeSize(const int &i) {
	__int64 rs;
	SHGetDiskFreeSpaceEx(this->pszDriveLetter[i], NULL, NULL, (PULARGE_INTEGER)&rs);
	return rs;
}

LPWSTR Drive::FreeSizeToStr(const int &i) {
	return Convert(GetFreeSize(i));
}
void Drive::GetSystemDrives() {
	TCHAR buffer[BUFFER_LEN]; // chứa chuỗi cho biết các ổ đĩa hiện hành

	GetLogicalDriveStrings(BUFFER_LEN, buffer);
	this->nCount = 0;
	// Đếm số lượng ổ đĩa
	// Nếu gặp 2 ký tự kết thúc liên tiếp -> hết chuỗi
	for (int i = 0; (buffer[i] != 0 || buffer[i + 1] != 0); i++) {
		// Nếu vị trí thứ i = 0 và i + 1 khác 0 thì trước đó có 1 đĩa
		if (buffer[i] == 0) {
			this->nCount++;
		}
	}
	this->nCount++;
	// Cấp phát các mảng chứa thông tin với từng đĩa
	this->pszDriveLetter = new TCHAR*[this->nCount];
	this->pszVolumeLabel = new TCHAR*[this->nCount];
	this->pszDisplayName = new TCHAR*[this->nCount];
	this->pszDriveType = new TCHAR*[this->nCount];

	for (int i = 0; i < this->nCount; i++) {
		this->pszDriveLetter[i] = new TCHAR[4]; // C:\,...
		this->pszVolumeLabel[i] = new TCHAR[31]; //Local Disk, Data,...
		this->pszDisplayName[i] = new TCHAR[50]; // Data(D:)
		this->pszDriveType[i] = new TCHAR[30];
	}

	// Lấy từng ký tự của ổ đĩa vd: C:\, D:\,...
	int j = 0; // chỉ số của buffer
	for (int i = 0; i < this->nCount; i++) {
		int k = 0;
		while (buffer[j] != 0) {
			this->pszDriveLetter[i][k++] = buffer[j++];
		}
		this->pszDriveLetter[i][k] = 0;
		j++;
	}

	// Lấy thông tin ổ đĩa
	for (int i = 0; i < this->nCount; i++) {
		int type = GetDriveType(this->pszDriveLetter[i]);

		//Assign type
		switch (type)
		{
		case DRIVE_FIXED:
			StrCpy(this->pszDriveType[i], DR_FIXED);
			break;
		case DRIVE_REMOVABLE:
			StrCpy(this->pszDriveType[i], DR_REMOVABLE);
			break;
		case DRIVE_REMOTE:
			StrCpy(this->pszDriveType[i], DR_REMOTE);
			break;
		case DRIVE_CDROM:
			StrCpy(this->pszDriveType[i], DR_CDROM);
			break;
		default:
			break;
		}

		if ((type == DRIVE_FIXED) || (type == DRIVE_REMOVABLE) || DRIVE_REMOTE) { // đĩa cứng, usb, mạng
			GetVolumeInformation(this->pszDriveLetter[i], buffer, BUFFER_LEN, VOLUME_SERIAL_NUMBER, MAXIMUM_COMPONENT_LENGTH, FILE_SYSTEM_FLAGS, FILE_SYSTEM_NAME_BUFFER, FILE_SYSTEM_NAME_SIZE);
			if (wcslen(buffer) == 0) {
				if (type == DRIVE_REMOVABLE)
					StrCpy(this->pszVolumeLabel[i], L"USB Drive");
				else if (type == DRIVE_FIXED)
					StrCpy(this->pszVolumeLabel[i], L"Local Disk");
			}
			else {
				StrCpy(this->pszVolumeLabel[i], buffer);
			}
		}
		else if (type == DRIVE_CDROM){ // ổ cd
			GetVolumeInformation(this->pszDriveLetter[i], buffer, BUFFER_LEN, VOLUME_SERIAL_NUMBER, MAXIMUM_COMPONENT_LENGTH, FILE_SYSTEM_FLAGS, FILE_SYSTEM_NAME_BUFFER, FILE_SYSTEM_NAME_SIZE);
			if (wcslen(buffer) == 0){ // không có label
				StrCpy(this->pszVolumeLabel[i], L"CD-ROM");
			}
			else {
				StrCpy(this->pszVolumeLabel[i], buffer);
			}
		}
		else if ((i == 0 || i == 1) && (type == DRIVE_REMOVABLE)) { // ổ đĩa thứ 0 và 1 là ổ đĩa mềm
			StrCpy(this->pszVolumeLabel[i], L"3½ Floppy");
		}

		// Tạo tên đầy đủ
		StrCpy(this->pszDisplayName[i], this->pszVolumeLabel[i]);
		StrCat(this->pszDisplayName[i], L" (");
		StrNCat(this->pszDisplayName[i], this->pszDriveLetter[i], 3);
		StrCat(this->pszDisplayName[i], L")");
	}
}

void SetIconFromShell32(HIMAGELIST* himgList) {
	HICON hIcon;
	HINSTANCE dllhinst;
	*himgList = ImageList_Create(GetSystemMetrics(SM_CXSMICON), GetSystemMetrics(SM_CYSMICON), ILC_COLOR32, 8, 8);
	ImageList_SetBkColor(*himgList, GetSysColor(COLOR_WINDOW));
	dllhinst = LoadLibrary(L"shell32.dll");
	// làm thủ công
	// thứ 0 là ThisPC
	hIcon = LoadIcon(dllhinst, MAKEINTRESOURCE(16));
	ImageList_AddIcon(*himgList, hIcon);
	// 1 -> Local Disk
	hIcon = LoadIcon(dllhinst, MAKEINTRESOURCE(9));
	ImageList_AddIcon(*himgList, hIcon);
	// 2-> Folder
	hIcon = LoadIcon(dllhinst, MAKEINTRESOURCE(4));
	ImageList_AddIcon(*himgList, hIcon);
	// 3-> USB
	hIcon = LoadIcon(dllhinst, MAKEINTRESOURCE(8));
	ImageList_AddIcon(*himgList, hIcon);
	// 4-> Network Drive
	hIcon = LoadIcon(dllhinst, MAKEINTRESOURCE(10));
	ImageList_AddIcon(*himgList, hIcon);
	// 5-> CD-ROM
	hIcon = LoadIcon(dllhinst, MAKEINTRESOURCE(12));

}


