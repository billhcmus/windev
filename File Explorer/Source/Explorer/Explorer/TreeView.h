#ifndef _TREEVIEW_H_
#define _TREEVIEW_H_
#include "Drive.h"
#pragma comment(lib, "comctl32.lib")

HWND CreateATreeView(HWND hwndParent, long ID, HINSTANCE hInst, long lExtStyle, int x, int y, int nWidth, int nHeight, long lStyle);
void loadThisPCToTree(Drive*, HWND);
void LoadTreeViewItem(HTREEITEM &, LPCWSTR, HWND);
LPCWSTR GetPath(HTREEITEM, HWND);
void ExpandChild(HWND, HTREEITEM);
#endif